# Hello Rails App

Starter Rails app made for Mike Hartl's
[tutorial](https://www.railstutorial.org/book/beginning#code-hello_action)

## Ruby Version

- 2.5.1

## Installation

- Run `bundle install` in directory

- Start development server by running `rails server` in directory
